package com.ryanair.api.connector.configuration;

import com.ryanair.api.connector.clients.Client;
import com.ryanair.api.connector.clients.RouteClient;
import com.ryanair.api.connector.clients.TimeTableClient;
import com.ryanair.api.connector.service.InterConnectorService;
import com.ryanair.api.connector.service.InterConnectorServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Profile("interconnection")
@Configuration
public class InterconnectionsConfiguration {

    @Bean(name="routeClient")
    @Primary
    public Client getRouteClient() {
        return mock(RouteClient.class);
    }

    @Bean(name="timeTableClient")
    @Primary
    public Client getTimeTableClient() {
        return mock(TimeTableClient.class);
    }

    @Bean("interConnectorService")
    public InterConnectorService getInterConnectorService() {
        return new InterConnectorServiceImpl(getRouteClient(), getTimeTableClient());
    }
}