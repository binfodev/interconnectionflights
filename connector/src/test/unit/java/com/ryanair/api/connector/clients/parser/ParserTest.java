package com.ryanair.api.connector.clients.parser;

import com.ryanair.api.connector.AbstractTest;
import com.ryanair.api.connector.core.data.clients.Route;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.ryanair.api.connector.core.factory.ParserFactory.getRoute;
import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("routeClient")
@TestPropertySource(locations = "classpath:test.properties")
public class ParserTest extends AbstractTest {

    @Value("${routes.example}")
    private String routesExample;

    private Route actual;

    @Before
    public void setUp() {
        actual = getRoute().parse(routesExample).get(0);
    }

    @Test
    public void routeParserSizeTest() {
        assertEquals(1, getRoute().parse(routesExample).size());
    }

    @Test
    public void routeAirportFromValidTest() {
        assertEquals("BRU", actual.getAirportFrom());
    }

    @Test
    public void routeAirportToValidTest() {
        assertEquals("LIS", actual.getAirportTo());
    }
}