package com.ryanair.api.connector;

import com.ryanair.api.connector.clients.requests.InterConnectionRequest;
import com.ryanair.api.connector.clients.requests.InterConnectionRequestImpl;
import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.DataTimeTable;
import com.ryanair.api.connector.core.data.Leg;
import com.ryanair.api.connector.core.data.clients.Day;
import com.ryanair.api.connector.core.data.clients.Flight;
import com.ryanair.api.connector.core.data.clients.Route;
import com.ryanair.api.connector.core.data.clients.TimeTable;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.reverse;
import static com.ryanair.api.connector.core.data.DateTimeUtil.parseDateTime;

public class AbstractTest {

    public static List<Chain> getChains(String... params) {
        List<Chain> chains = newArrayList();
        Chain chain = new Chain();
        for (int i = 0; i < params.length - 1; i++) {
            addRoute(chain, params[i], params[i + 1]);
        }
        if (params.length >= 2) {
            chains.add(chain);
        }
        return chains;
    }

    public static Route addRoute(String from, String to) {
        Route route = new Route();
        route.setAirportFrom(from);
        route.setAirportTo(to);
        return route;
    }

    public static void addRoute(Chain chain, String from, String to) {
        Route route = new Route();
        route.setAirportFrom(from);
        route.setAirportTo(to);
        chain.getItems().add(route);
    }

    protected TimeTable getDefaultTimeTable() {
        TimeTable timeTable = new TimeTable();
        addDay(timeTable, 1)
                .addFlight(timeTable.getDays().get(0), "07:00", "10:00");
        addDay(timeTable, 2)
                .addFlight(timeTable.getDays().get(1), "07:00", "10:00");
        timeTable.setMonth(2);
        return timeTable;
    }

    protected InterConnectionRequest getRouteRequest(String from, String to) {
        return getRequest(from, "2016-03-01T09:30", to, "2016-03-01T17:00");
    }

    protected InterConnectionRequest getRequest(String fromDate, String toDate) {
        return getRequest("from", fromDate, "to", toDate);
    }

    protected InterConnectionRequest getRequest(String from, String fromDate, String to, String toDate) {
        return new InterConnectionRequestImpl(from, parseDateTime(fromDate), to, parseDateTime(toDate));
    }

    public DataTimeTable getExpected(String... params) {
        List<Leg> legs = newArrayList();
        for (int i = 0; i <= params.length - 2; i += 2) {
            Leg leg = getLeg(params[i], params[i + 1]);
            legs.add(leg);
        }
        return new DataTimeTable(legs);
    }

    protected Leg getLeg(String from, String to) {
        return new Leg("from", parseDateTime(from), "to", parseDateTime(to));
    }

    protected AbstractTest addFlight(Day day, String from, String to) {
        Flight flight = new Flight();
        flight.setDepartureTime(from);
        flight.setArrivalTime(to);
        day.getFlights().add(flight);
        return this;
    }

    protected AbstractTest addDay(TimeTable timeTable, int dayNumber) {
        Day day = new Day();
        day.setDay(dayNumber);
        timeTable.getDays().add(day);
        return this;
    }
}