package com.ryanair.api.connector.service;

import com.ryanair.api.connector.AbstractTest;
import com.ryanair.api.connector.clients.Client;
import com.ryanair.api.connector.clients.requests.InterConnectionRequest;
import com.ryanair.api.connector.clients.requests.RequestRoute;
import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.DataTimeTable;
import com.ryanair.api.connector.core.data.InterConnection;
import com.ryanair.api.connector.core.data.Leg;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.ryanair.api.connector.core.data.DateTimeUtil.parseDateTime;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("interconnection")
@TestPropertySource(locations = "classpath:test.properties")
public class InterConnectionServiceTest extends AbstractTest {

    private InterConnectorService service;

    @Autowired
    @Qualifier("routeClient")
    private Client<RequestRoute, List<Chain>> routeClient;

    @Autowired
    @Qualifier("timeTableClient")
    private Client<InterConnectionRequest, DataTimeTable> tableClient;

    @Before
    public void setUp() {
        service = new InterConnectorServiceImpl(routeClient, tableClient);
    }

    @Test
    public void interConnectionsDirectTest() {
        mockRoutes("WRO", "STN");
        List<Leg> legs = newArrayList();
        legs.add(getLeg("DUB", "2016-03-01T09:30", "WRO", "2016-03-01T11:30"));
        mockTimeTable(legs);
        List<InterConnection> interConnections = service.getInterConnections(getRouteRequest("WRO", "STN"));
        assertEquals(newArrayList(new InterConnection(legs)), interConnections);
    }

    @Test
    public void interConnectionsMultipleTest() {
        mockRoutes("DUB", "STN", "WRO");
        List<Leg> legs = newArrayList();
        legs.add(getLeg("DUB", "2016-03-01T09:30", "STN", "2016-03-01T11:30"));
        legs.add(getLeg("STN", "2016-03-01T16:00", "WRO", "2016-03-01T17:00"));
        mockTimeTable(legs);
        List<InterConnection> interConnections = service.getInterConnections(getRequest("DUB", "2016-03-01T09:30", "WRO", "2016-03-01T17:00"));
        assertEquals(newArrayList(new InterConnection(legs)), interConnections);
    }

    @Test
    public void interConnectionsMultipleNoTimeTableTest() {
        mockRoutes("DUB", "STN", "WRO");
        mockTimeTable();
        List<InterConnection> interConnections = service.getInterConnections(getRouteRequest("DUB", "WRO"));
        assertEquals(newArrayList(), interConnections);
    }

    @Test
    public void interConnectionsMultipleNoRoutesTest() {
        mockRoutes();
        mockTimeTable(newArrayList(getLeg("DUB", "2018-03-23T12:30", "WRO", "2018-03-23T13:30")));
        List<InterConnection> interConnections = service.getInterConnections(getRouteRequest("DUB", "WRO"));
        assertEquals(newArrayList(), interConnections);
    }

    @Test
    public void interConnectionsMultipleTimeBetweenStopsTest() {
        mockRoutes("DUB", "STN", "WRO");
        List<Leg> legs = newArrayList();
        legs.add(getLeg("DUB", "2018-03-23T12:30", "STN", "2018-03-23T13:00"));
        legs.add(getLeg("STN", "2018-03-23T14:59", "WRO", "2018-03-23T14:59"));
        mockTimeTable(legs);
        List<InterConnection> interConnections = service.getInterConnections(getRouteRequest("DUB", "WRO"));
        assertEquals(newArrayList(), interConnections);
    }

    @Test
    public void interConnectionsMultipleTimeBetweenStopsNotFilteredTest() {
        List<Leg> legs = newArrayList();
        legs.add(getLeg("DUB", "2018-03-23T12:30", "STN", "2018-03-23T13:00"));
        legs.add(getLeg("STN", "2018-03-23T15:01", "WRO", "2018-03-23T14:59"));
        DataTimeTable timeTable = new DataTimeTable(legs);
        assertEquals(legs, timeTable.getTimes());
    }

    private void mockRoutes(String... params) {
        if (params != null && params.length != 0) {
            List<Chain> chains = getChains(params);
            when(routeClient.get(any())).thenReturn(chains);
        } else {
            when(routeClient.get(any())).thenReturn(newArrayList());
        }
    }

    private void mockTimeTable(String... params) {
        when(tableClient.get(any())).thenReturn(new DataTimeTable(getExpectedInterConnection(params).stream().flatMap(f -> f.getLegs().stream()).collect(Collectors.toList())));
    }

    private void mockTimeTable(List<Leg> legs) {
        when(tableClient.get(any())).thenReturn(new DataTimeTable(legs));
    }

    public List<InterConnection> getExpectedInterConnection(String... params) {
        List<InterConnection> interConnections = newArrayList();

        List<Leg> legs = newArrayList();
        for (int i = 0; i < params.length - 1; i++) {
            legs.add(getLeg(params[i], params[i + 1]));
        }
        if (params.length >= 2) {
            interConnections.add(new InterConnection(legs));
        }
        return interConnections;
    }

    private Leg getLeg(String from, String fromTime, String to, String toTime) {
        return new Leg(from, parseDateTime(fromTime), to, parseDateTime(toTime));
    }
}