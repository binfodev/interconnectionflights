package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.AbstractTest;
import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.clients.provider.ClientProvider;
import com.ryanair.api.connector.clients.requests.InterConnectionRequest;
import com.ryanair.api.connector.core.data.DataTimeTable;
import com.ryanair.api.connector.core.data.clients.TimeTable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"timetableClient"})
@TestPropertySource("classpath:test.properties")
public class TimeTableClientTest extends AbstractTest {

    private Client<InterConnectionRequest, DataTimeTable> client;

    @Autowired
    @Qualifier("timetableProvider")
    private ClientProvider provider;

    @Autowired
    @Qualifier("timeTableParser")
    private Parser<TimeTable> parser;

    @Value("${schedulesUrl}")
    String template;

    @Before
    public void setUp() {
        client = new TimeTableClient(new UrlResolverImpl(template), provider, parser);
        when(provider.receive(any())).thenReturn("");
    }

    @Test
    public void getTimeTableTest() {
        when(parser.parse(any())).thenReturn(getDefaultTimeTable());

        DataTimeTable actual = client.get(getRequest("2016-03-01T06:00", "2016-03-01T10:00"));
        DataTimeTable expected = getExpected("2016-03-01T07:00", "2016-03-01T10:00");

        assertEquals(expected, actual);
    }

    @Test
    public void getLateDeparture() {
        when(parser.parse(any())).thenReturn(getDefaultTimeTable());
        DataTimeTable actual = client.get(getRequest("2016-03-01T07:01", "2016-03-01T10:00"));
        assertEquals(newArrayList(), actual.getTimes());
    }

    @Test
    public void getLateArrival() {
        when(parser.parse(any())).thenReturn(getDefaultTimeTable());
        DataTimeTable actual = client.get(getRequest("2016-03-01T07:00", "2016-03-01T09:59"));
        assertEquals(newArrayList(), actual.getTimes());
    }

    @Test
    public void getMultiplyTest() {
        TimeTable timeTable = getDefaultTimeTable();
        addFlight(timeTable.getDays().get(0), "08:30", "11:30");
        addFlight(timeTable.getDays().get(0), "09:30", "11:30");
        addFlight(timeTable.getDays().get(0), "14:00", "16:00");
        addFlight(timeTable.getDays().get(0), "14:00", "17:00");
        when(parser.parse(any())).thenReturn(timeTable);

        DataTimeTable actual = client.get(getRequest("2016-03-01T09:00", "2016-03-01T17:00"));
        DataTimeTable expected = getExpected("2016-03-01T09:30", "2016-03-01T11:30", "2016-03-01T14:00", "2016-03-01T16:00");
        assertNotEquals(expected, actual);
    }

    @Test
    public void urlResolverTest() {
        UrlResolver resolver = new UrlResolverImpl(template);
        String expected = "api.ryanair.com/timetable/3/schedules/DUB/WRO/years/2016/months/2";
        String actual = resolver.resolve(getRequest("DUB", "2016-03-01T09:00", "WRO", "2016-03-01T17:00"));
        assertEquals(expected, actual);
    }
}