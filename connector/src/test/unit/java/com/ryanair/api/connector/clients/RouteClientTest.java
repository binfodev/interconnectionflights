package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.clients.provider.ClientProvider;
import com.ryanair.api.connector.clients.requests.RequestRoute;
import com.ryanair.api.connector.clients.requests.RequestRouteImpl;
import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.clients.Route;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.ryanair.api.connector.AbstractTest.addRoute;
import static com.ryanair.api.connector.AbstractTest.getChains;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("routeClient")
@TestPropertySource("classpath:test.properties")
public class RouteClientTest {

    private Client<RequestRoute, List<Chain>> client;

    @Autowired
    @Qualifier("routeProvider")
    private ClientProvider provider;

    @Autowired
    @Qualifier("routeParser")
    private Parser<List<Route>> parser;

    @Value("${routesUrl}")
    private String url;

    @Before
    public void setUp() {
        client = new RouteClient(url, provider, parser);
        when(provider.receive(any())).thenReturn("");
    }

    @Test
    public void directTest() {
        when(parser.parse(any())).thenReturn(getChains("DUB", "WRO", "CAT", "TES").get(0).getItems());
        List<Chain> actual = client.get(new RequestRouteImpl("DUB", "WRO"));
        assertEquals(getChains("DUB", "WRO"), actual);
    }

    @Test
    public void multipleRouteTest() {
        List<Route> routes = getChains("DUB1", "WRO2", "WAR3").get(0).getItems();
        routes.add(addRoute( "WRT", "TTR"));
        routes.add(addRoute( "STR", "DRT"));
        routes.add(addRoute( "DUB1", "DRT"));
        when(parser.parse(any())).thenReturn(routes);
        List<Chain> actual = client.get(new RequestRouteImpl( "DUB1", "WAR3"));
        assertEquals(getChains("DUB1", "WRO2", "WAR3"), actual);
    }

    @Test
    public void departureNotExistsTest() {
        when(parser.parse(any())).thenReturn(getChains("WRO", "WAR").get(0).getItems());
        List<Chain> actual = client.get(new RequestRouteImpl("DUB", "WAR"));
        assertEquals(newArrayList(), actual);
    }

    @Test
    public void arrivalNotExistsTest() {
        when(parser.parse(any())).thenReturn(getChains("DUB", "WRO").get(0).getItems());
        List<Chain> actual = client.get(new RequestRouteImpl( "DUB", "WAR"));
        assertEquals(newArrayList(), actual);
    }

    @Test
    public void multipleChainsTest() {
        List<Chain> chains = getChains("DUB", "WRO", "WAR");
        chains.add(getChains("DUB", "WAR").get(0));
        List<Route> routes = chains.stream().flatMap(i -> i.getItems().stream()).collect(Collectors.toList());
        when(parser.parse(any())).thenReturn(routes);

        List<Chain> actual = client.get(new RequestRouteImpl( "DUB", "WAR"));

        assertEquals(chains, actual);
    }
}