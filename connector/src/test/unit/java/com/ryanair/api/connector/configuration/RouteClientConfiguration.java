package com.ryanair.api.connector.configuration;

import com.ryanair.api.connector.clients.Client;
import com.ryanair.api.connector.clients.RouteClient;
import com.ryanair.api.connector.clients.TimeTableClient;
import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.clients.provider.ClientProvider;
import com.ryanair.api.connector.clients.provider.ClientProviderImpl;
import com.ryanair.api.connector.core.factory.ParserFactory;
import com.ryanair.api.connector.service.InterConnectorService;
import com.ryanair.api.connector.service.InterConnectorServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Profile("routeClient")
@Configuration
public class RouteClientConfiguration {

    @Bean(name = "routeProvider")
    @Primary
    public ClientProvider getRouteProvider() {
        return mock(ClientProviderImpl.class);
    }

    @Bean(name = "routeParser")
    @Primary
    public Parser getRouteParser() {
        return mock(Parser.class);
    }

    @Bean("interConnectorService")
    public InterConnectorService getInterConnectorService() {
        return new InterConnectorServiceImpl(mock(RouteClient.class), mock(TimeTableClient.class));
    }
}
