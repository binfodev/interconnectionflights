package com.ryanair.api.connector;

import com.ryanair.api.connector.clients.Client;
import com.ryanair.api.connector.clients.requests.RequestRoute;
import com.ryanair.api.connector.clients.requests.RequestRouteImpl;
import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.factory.ParserFactory;
import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.core.data.clients.TimeTable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("application")
public class ClientsTest {

    @Autowired
    @Qualifier("routeClient")
    private Client<RequestRoute, List<Chain>> routeClient;

    @Test
    public void parseRouteTest() {
        System.out.println("");
        RequestRoute req = new RequestRouteImpl("DUB", "WRO");

        List<Chain> routes = routeClient.get(req);
        assertFalse(routes.isEmpty());

        routes.forEach(r -> r.getItems().forEach(i -> assertNotNull(i.getAirportFrom())));
        routes.forEach(r -> r.getItems().forEach(i -> assertNotNull(i.getAirportFrom())));
    }

    @Test
    public void parseTimeTableTest() {
        Parser<TimeTable> parser = ParserFactory.getTimeTable();
        TimeTable timeTables = parser.parse("https://api.ryanair.com/timetable/3/schedules/DUB/WRO/years/2018/months/3");
        timeTables.getDays().forEach(f -> {
            assertNotNull(f);
            f.getFlights().forEach(Assert::assertNotNull);
        });
    }
}