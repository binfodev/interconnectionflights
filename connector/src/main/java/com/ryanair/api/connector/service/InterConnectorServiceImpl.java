package com.ryanair.api.connector.service;

import com.ryanair.api.connector.clients.Client;
import com.ryanair.api.connector.clients.requests.*;
import com.ryanair.api.connector.core.data.*;
import com.ryanair.api.connector.core.data.clients.Route;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class InterConnectorServiceImpl implements InterConnectorService {
    private static final Logger LOG = LoggerFactory.getLogger(InterConnectorService.class);

    private Client<RequestRoute, List<Chain>> requestRoute;
    private Client<InterConnectionRequest, DataTimeTable> requestTimeTable;

    public InterConnectorServiceImpl(Client<RequestRoute, List<Chain>> requestRoute, Client<InterConnectionRequest, DataTimeTable> requestTimeTable) {
        this.requestRoute = requestRoute;
        this.requestTimeTable = requestTimeTable;
    }

    @Override
    public List<InterConnection> getInterConnections(InterConnectionRequest request) {
        InterConnectorManager manager = new InterConnectorManager();
        manager.setChains(requestRoute.get(request));
        return getaAllInterConnections(manager, request);
    }

    private List<InterConnection> getaAllInterConnections(InterConnectorManager manager, RequestTimeTable request) {
        for (Chain chain : manager.getChains()) {
            for (Route route : chain.getItems()) {
                try {
                    manager.addInterConnection(chain, getTimeTable(route, request).getTimes());
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
        return manager.getTarget();
    }

    private DataTimeTable getTimeTable(Route t, RequestTimeTable request) {
        return requestTimeTable.get(new InterConnectionRequestImpl(t, request));
    }
}