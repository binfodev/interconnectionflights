package com.ryanair.api.connector.clients.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class ClientProviderImpl implements ClientProvider {
    private static final Logger LOG = LoggerFactory.getLogger(ClientProvider.class);

    @Override
    public String receive(String url) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(URI.create(url), String.class);
            return response.getBody();
        } catch (RestClientResponseException e) {
            String message = String.format("error while getting request [%s] exception %s", url, e.getMessage());
            LOG.error("error while getting request {} exception {}", url, e);
            throw new RuntimeException(message, e);
        }
    }
}