package com.ryanair.api.connector.core.data.clients;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Day {
    private Integer day;
    private List<Flight> flights = newArrayList();

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        return "Day{" +
                "day=" + day +
                ", flights=" + flights +
                '}';
    }
}