package com.ryanair.api.connector.core.data;

import java.util.List;
import java.util.Objects;

import static com.google.common.collect.Lists.newLinkedList;

public class InterConnection {

    private List<Leg> legs = newLinkedList();

    public InterConnection(List<Leg> legs) {
        this.legs = legs;
    }

    public InterConnection() {
    }

    public Integer getStops() {
        return legs.size() - 1;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InterConnection that = (InterConnection) o;
        return Objects.equals(legs, that.legs);
    }

    @Override
    public int hashCode() {

        return Objects.hash(legs);
    }

    @Override
    public String toString() {
        return "InterConnection{" +
                "legs=" + legs +
                '}';
    }
}