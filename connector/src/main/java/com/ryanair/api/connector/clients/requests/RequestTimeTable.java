package com.ryanair.api.connector.clients.requests;

import java.util.Date;

public interface RequestTimeTable extends Request {

    Date getDepartureDate();
    Date getArrivalDate();
}