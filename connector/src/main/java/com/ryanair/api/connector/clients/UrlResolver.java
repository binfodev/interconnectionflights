package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.clients.requests.InterConnectionRequest;

public interface UrlResolver {

    String resolve(InterConnectionRequest request);
}
