package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.clients.provider.ClientProvider;
import com.ryanair.api.connector.clients.requests.InterConnectionRequest;
import com.ryanair.api.connector.core.data.DataTimeTable;
import com.ryanair.api.connector.core.data.Leg;
import com.ryanair.api.connector.core.data.clients.TimeTable;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.ryanair.api.connector.core.data.DateTimeUtil.parseDateTime;

public class TimeTableClient implements Client<InterConnectionRequest, DataTimeTable> {

    private ClientProvider provider;
    private Parser<TimeTable> parser;
    private UrlResolver resolver;

    public TimeTableClient(UrlResolver resolver, ClientProvider provider, Parser<TimeTable> parser) {
        this.parser = parser;
        this.provider = provider;
        this.resolver = resolver;
    }

    @Override
    public DataTimeTable get(InterConnectionRequest request) {
        List<InterConnectionRequest> requests = prepareRequests(request);
        List<Leg> legs = newArrayList();
        for (InterConnectionRequest internalRequest : requests) {
            TimeTable timeTable = getTimeTable(internalRequest);
            List<Leg> leg = getFilteredLeg(timeTable, internalRequest);
            legs.addAll(leg);
        }
        return new DataTimeTable(legs);
    }

    private List<InterConnectionRequest> prepareRequests(InterConnectionRequest request) {
        return newArrayList(request);
    }

    private TimeTable getTimeTable(InterConnectionRequest request) {
        String message = provider.receive(resolver.resolve(request));
        return parser.parse(message);
    }

    private List<Leg> getFilteredLeg(TimeTable timeTable, InterConnectionRequest request) {
        return getLegs(timeTable, request).stream()
                .filter(leg -> filterEarlyDepartures(request, leg))
                .filter(leg -> filterLaterArrivals(request, leg))
                .collect(Collectors.toList());
    }

    private boolean filterEarlyDepartures(InterConnectionRequest request, Leg leg) {
        return !leg.getDepartureDateTime().before(request.getDepartureDate());
    }

    private boolean filterLaterArrivals(InterConnectionRequest request, Leg leg) {
        return !leg.getArrivalDateTime().after(request.getArrivalDate());
    }

    private List<Leg> getLegs(TimeTable timeTable, InterConnectionRequest request) {
        List<Leg> legs = newArrayList();
        Integer month = timeTable.getMonth();
        timeTable.getDays().forEach(d -> d.getFlights().forEach(f -> legs.add(
                new Leg(request.getDeparture(), parseDateTime(request.getDepartureDate(), month, d.getDay(), f.getDepartureTime()),
                        request.getArrival(), parseDateTime(request.getArrivalDate(), month, d.getDay(), f.getArrivalTime())))));
        return legs;
    }
}