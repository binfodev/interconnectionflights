package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.clients.provider.ClientProvider;
import com.ryanair.api.connector.clients.requests.RequestRoute;
import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.clients.Route;

import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newLinkedHashMap;
import static com.google.common.collect.Sets.*;

public class RouteClient implements Client<RequestRoute, List<Chain>> {

    private ClientProvider provider;
    private Parser<List<Route>> parser;

    private String url;

    public RouteClient(String url, ClientProvider provider, Parser<List<Route>> parser) {
        this.url = url;
        this.provider = provider;
        this.parser = parser;
    }

    @Override
    public List<Chain> get(RequestRoute request) {
        String message = provider.receive(url);
        List<Route> routes = parser.parse(message);
        return findRouteChains(routes, request);
    }

    private List<Chain> findRouteChains(List<Route> routes, RequestRoute request) {
        RouteManager manager = new RouteManager();
        manager.setDepartures(routes);
        manager.setArrivals(routes);

        int chainMaxSize = 3;
        for (int i = 2; i <= chainMaxSize; i++) {
            enrichMiddleRoutes(manager, createTemplate(request, i));
        }
        return manager.getTarget();
    }

    private void enrichMiddleRoutes(RouteManager managerAllRoutes, String[] template) {
        Map<Integer, RouteManager> managers = newLinkedHashMap();

        for (int i = 0; i <= template.length / 2; i++) {
            if (i == 0) {
                collectFirstManagerByDepartures(managerAllRoutes, template[0], managers);
                collectLastManagerByArrivals(managerAllRoutes, template, managers);
            } else {
                mergeNextManagerWithPrevious(managerAllRoutes, template, managers, i);
                mergePreviousManagerWithNext(managerAllRoutes, template, managers, i);
            }
        }
        collectChains(managerAllRoutes, template, managers);
    }

    private void collectChains(RouteManager managerAllRoutes, String[] template, Map<Integer, RouteManager> managers) {
        Iterator<Route> dep = newArrayList(managers.get(0).getDepartures().getOrDefault(template[0], newLinkedHashSet())).iterator();
        Iterator<Route> arr = newArrayList(managers.get(template.length - 1).getArrivals().getOrDefault(template[template.length - 1], newLinkedHashSet())).iterator();
        Chain chain;
        while (dep.hasNext() && arr.hasNext()) {
            chain = new Chain();
            Set<Route> routes = newLinkedHashSet();
            Route d = dep.next();
            Route a = arr.next();
            routes.add(d);
            routes.add(a);
            chain.getItems().addAll(routes);
            managerAllRoutes.putTarget(chain);
        }
    }

    private void mergePreviousManagerWithNext(RouteManager managerAllRoutes, String[] template, Map<Integer, RouteManager> managers, int i) {
        RouteManager manager = managers.getOrDefault(template.length - 1 - i, new RouteManager());
        List<String> prevDepartures = managers.get(template.length - 1 - i).getDepartures().values().stream().flatMap(Collection::stream)
                .map(Route::getAirportFrom).collect(Collectors.toList());
        List<Route> arrival = findAllArrivalsByList(managerAllRoutes, prevDepartures);
        manager.setArrivals(arrival);
        if (doNotUpdateOriginalDepartureArrival(template, i)) {
            manager.setDepartures(arrival);
        }
        managers.put(template.length - 1 - i, manager);
    }

    private void mergeNextManagerWithPrevious(RouteManager managerAllRoutes, String[] template, Map<Integer, RouteManager> managers, int i) {
        RouteManager manager = managers.getOrDefault(i, new RouteManager());
        List<String> prevArrival = managers.get(i - 1).getArrivals().values().stream().flatMap(Collection::stream)
                .map(Route::getAirportTo).collect(Collectors.toList());
        List<Route> departures = findAllDeparturesByList(managerAllRoutes, prevArrival);

        manager.setDepartures(departures);
        if (doNotUpdateOriginalDepartureArrival(template, i)) {
            manager.setArrivals(departures);
        }
        managers.put(i, manager);
    }

    private boolean doNotUpdateOriginalDepartureArrival(String[] template, int i) {
        return !(i + 1 == template.length);
    }

    private void collectLastManagerByArrivals(RouteManager managerAllRoutes, String[] template, Map<Integer, RouteManager> managers) {
        RouteManager manager = managers.getOrDefault(template.length - 1, new RouteManager());
        String to = template[template.length - 1];
        List<Route> arrivals = findAllArrivalsByList(managerAllRoutes, newArrayList(to));
        manager.setArrivals(to, arrivals);
        manager.setDepartures(arrivals);
        managers.put(template.length - 1, manager);
    }

    private void collectFirstManagerByDepartures(RouteManager managerAllRoutes, String from, Map<Integer, RouteManager> managers) {
        RouteManager manager = managers.getOrDefault(0, new RouteManager());

        List<Route> departures = findAllDeparturesByList(managerAllRoutes, newArrayList(from));
        manager.setDepartures(from, departures);
        manager.setArrivals(departures);
        managers.put(0, manager);
    }

    private List<Route> findAllArrivalsByList(RouteManager managerAllRoutes, List<String> strings) {
        List<Route> target = newArrayList();
        strings.forEach(k -> target.addAll(managerAllRoutes.getArrivals().getOrDefault(k, newLinkedHashSet())));
        return target;
    }

    private List<Route> findAllDeparturesByList(RouteManager managerAllRoutes, List<String> strings) {
        List<Route> target = newArrayList();
        strings.forEach(k -> target.addAll(managerAllRoutes.getDepartures().getOrDefault(k, newLinkedHashSet())));
        return target;
    }

    private String[] createTemplate(RequestRoute request, int size) {
        String[] template = new String[size];
        template[0] = request.getDeparture();
        template[size - 1] = request.getArrival();
        return template;
    }
}