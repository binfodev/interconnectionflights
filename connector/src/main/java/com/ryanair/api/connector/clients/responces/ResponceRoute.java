package com.ryanair.api.connector.clients.responces;

import com.ryanair.api.connector.core.data.clients.Route;

import java.util.List;

public class ResponceRoute {

    List<Route> routes;

    public ResponceRoute(List<Route> routes) {
        this.routes = routes;
    }

    public List<Route> getRoutes() {
        return routes;
    }
}
