package com.ryanair.api.connector.clients.requests;

public interface RequestRoute extends Request {

    String getDeparture();
    String getArrival();
}