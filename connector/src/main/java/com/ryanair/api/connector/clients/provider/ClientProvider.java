package com.ryanair.api.connector.clients.provider;

@FunctionalInterface
public interface ClientProvider {

    String receive(String url);
}