package com.ryanair.api.connector.service;

import com.ryanair.api.connector.clients.requests.InterConnectionRequest;
import com.ryanair.api.connector.core.data.InterConnection;

import java.util.List;

public interface InterConnectorService {

    List<InterConnection> getInterConnections(InterConnectionRequest request);
}
