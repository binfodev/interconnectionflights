package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.clients.Route;

import java.util.*;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static com.google.common.collect.Sets.newLinkedHashSet;

class RouteManager {

    private Map<String, Chain> target = newLinkedHashMap();
    private Map<String, Set<Route>> departures = newLinkedHashMap();
    private Map<String, Set<Route>> arrivals = newLinkedHashMap();

    public List<Chain> getTarget() {
        return new ArrayList<>(target.values());
    }
    public void putTarget(Chain chain) {
        target.put(chain.toString(), chain);
    }

    public void setDepartures(String key, List<Route> routes) {
         departures.put(key, newLinkedHashSet(routes));
    }

    public void setDepartures(List<Route> departures) {
        departures.forEach(this::addDeparture);
    }

    private void addDeparture(Route d) {
        Set<Route> routes = departures.getOrDefault(d.getAirportFrom(), newLinkedHashSet());
        routes.add(d);
        departures.put(d.getAirportFrom(), routes);
    }

    public void setArrivals(String key, List<Route> arrivals) {
        this.arrivals.put(key, newLinkedHashSet(arrivals));
    }

    public void setArrivals(List<Route> arrivals) {
        arrivals.forEach(this::addArrival);
    }

    public Map<String, Set<Route>> getDepartures() {
        return departures;
    }

    public Map<String, Set<Route>> getArrivals() {
        return arrivals;
    }

    private void addArrival(Route route) {
        Set<Route> routes = arrivals.getOrDefault(route.getAirportTo(), newLinkedHashSet());
        routes.add(route);
        arrivals.put(route.getAirportTo(), routes);
    }
}