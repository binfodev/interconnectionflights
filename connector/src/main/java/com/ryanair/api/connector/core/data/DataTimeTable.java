package com.ryanair.api.connector.core.data;

import java.util.List;
import java.util.Objects;

import static com.google.common.collect.Lists.newArrayList;
import static com.ryanair.api.connector.core.data.DateTimeUtil.before;

public class DataTimeTable {

    private List<Leg> times;

    public DataTimeTable(List<Leg> times) {
        this.times = newArrayList();
        addAll(times);
    }

    private void addAll(List<Leg> items) {
        if (isvalid(items)) {
            times.addAll(items);
        }
    }

    private boolean isvalid(List<Leg> legs) {
        List<Leg> temp = newArrayList(times);
        for (Leg leg : legs) {
            if (temp.isEmpty()) {
                temp.add(leg);
            } else {
                Leg last = temp.get(temp.size() - 1);
                if (before(last.getArrivalDateTime(), leg.getDepartureDateTime(), 2)) {
                    temp.add(leg);
                }
            }
        }
        return temp.size() == legs.size();
    }

    public List<Leg> getTimes() {
        return times;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataTimeTable table = (DataTimeTable) o;
        return Objects.equals(times, table.times);
    }

    @Override
    public int hashCode() {

        return Objects.hash(times);
    }

    @Override
    public String toString() {
        return "DataTimeTable{" +
                "times=" + times +
                '}';
    }
}