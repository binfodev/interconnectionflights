package com.ryanair.api.connector.core.data;

import com.ryanair.api.connector.core.data.clients.Route;

import java.util.List;
import java.util.Objects;

import static com.google.common.collect.Lists.newLinkedList;

public class Chain {

    private List<Route> routes = newLinkedList();

    public Chain() {
    }

    public Chain(List<Route> routes) {
        this.routes = routes;
    }

    public List<Route> getItems() {
        return routes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chain chain = (Chain) o;
        return Objects.equals(routes, chain.routes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(routes);
    }

    @Override
    public String toString() {
        return "Chain{" +
                "routes=" + routes +
                '}';
    }
}