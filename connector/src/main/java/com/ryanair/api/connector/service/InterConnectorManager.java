package com.ryanair.api.connector.service;

import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.InterConnection;
import com.ryanair.api.connector.core.data.Leg;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newLinkedHashSet;

public class InterConnectorManager {

    private Map<String, InterConnection> target = newHashMap();

    private List<Chain> chains;

    List<InterConnection> getTarget() {
        return newArrayList(target.values());
    }

    public void setChains(List<Chain> chains) {
        this.chains = chains;
    }

    public List<Chain> getChains() {
        return chains;
    }

    public void addInterConnection(Chain chain, List<Leg> times) {
        InterConnection ic = target.getOrDefault(chain.toString(), new InterConnection());
        Set<Leg> set = newLinkedHashSet(ic.getLegs());
        set.addAll(times);
        ic.getLegs().clear();
        ic.getLegs().addAll(set);
        if (ic.getLegs().isEmpty()) {
            target.remove(chain.toString());
        } else {
            target.put(chain.toString(), ic);
        }
    }
}