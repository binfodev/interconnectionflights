package com.ryanair.api.connector.clients.requests;

import java.util.Objects;

public final class RequestRouteImpl implements RequestRoute {

    private String from;
    private String to;

    public RequestRouteImpl(String from, String to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public String getDeparture() {
        return from;
    }

    @Override
    public String getArrival() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestRouteImpl that = (RequestRouteImpl) o;
        return Objects.equals(from, that.from) &&
                Objects.equals(to, that.to);
    }

    @Override
    public int hashCode() {

        return Objects.hash(from, to);
    }
}