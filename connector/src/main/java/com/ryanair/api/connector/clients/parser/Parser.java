package com.ryanair.api.connector.clients.parser;

public interface Parser<T> {

    T parse(String content);
}
