package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.clients.requests.InterConnectionRequest;

import static com.ryanair.api.connector.core.data.DateTimeUtil.getMonth;
import static com.ryanair.api.connector.core.data.DateTimeUtil.getYear;

public class UrlResolverImpl implements UrlResolver {

    private String urlPattern;

    public UrlResolverImpl(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    @Override
    public String resolve(InterConnectionRequest request) {
        return String.format(urlPattern, request.getDeparture(), request.getArrival(),
                getYear(request.getDepartureDate()), getMonth(request.getDepartureDate()));
    }
}
