package com.ryanair.api.connector.core.factory;

import com.ryanair.api.connector.clients.parser.JacksonParserImpl;
import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.core.data.clients.Route;
import com.ryanair.api.connector.core.data.clients.TimeTable;

import java.util.ArrayList;
import java.util.List;

public class ParserFactory {

    public static Parser<List<Route>> getRoute() {
        return new JacksonParserImpl<>(Route.class, ArrayList.class);
    }

    public static Parser<TimeTable> getTimeTable() {
        return new JacksonParserImpl<>(TimeTable.class);
    }
}