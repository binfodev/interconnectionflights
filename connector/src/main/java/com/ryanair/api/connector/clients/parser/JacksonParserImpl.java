package com.ryanair.api.connector.clients.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;

public class JacksonParserImpl<T> implements Parser<T> {
    private static final Logger LOG = LoggerFactory.getLogger(Parser.class);

    private CollectionType collectionType;
    private Class<T> type;

    public JacksonParserImpl(Class<?> type, Class<? extends Collection> collection) {
        this.collectionType = getCollectionType(collection, type);
    }

    public JacksonParserImpl(Class<T> type) {
        this.type = type;
    }

    @Override
    public T parse(String content) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            if (collectionType != null) {
                return mapper.readValue(content, collectionType);
            } else {
                return mapper.readValue(content, type);
            }
        } catch (IOException e) {
            LOG.error("error while processing content {} exception {}", content, e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private CollectionType getCollectionType(Class<? extends Collection> collection, Class item) {
        return new ObjectMapper().getTypeFactory().constructCollectionType(collection, item);
    }
}