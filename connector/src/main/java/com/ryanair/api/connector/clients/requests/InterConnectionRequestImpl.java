package com.ryanair.api.connector.clients.requests;

import com.ryanair.api.connector.core.data.clients.Route;

import java.util.Date;
import java.util.Objects;

public class InterConnectionRequestImpl implements InterConnectionRequest {

    private Route route;
    private RequestTimeTable timeTableRequest;

    public InterConnectionRequestImpl(Route route, RequestTimeTable timeTableRequest) {
        this.timeTableRequest = timeTableRequest;
        this.route = route;
    }

    public InterConnectionRequestImpl(String from, Date departureDate, String to, Date arrivalDate) {
        route = new Route();
        route.setAirportFrom(from);
        route.setAirportTo(to);
        timeTableRequest = new RequestTimeTableImpl(departureDate, arrivalDate);
    }

    public void setFrom(String from) {
        route.setAirportFrom(from);
    }

    public void setTo(String to){
        route.setAirportTo(to);
    }

    @Override
    public String getDeparture() {
        return route.getAirportFrom();
    }

    @Override
    public String getArrival() {
        return route.getAirportTo();
    }

    @Override
    public Date getDepartureDate() {
        return timeTableRequest.getDepartureDate();
    }

    @Override
    public Date getArrivalDate() {
        return timeTableRequest.getArrivalDate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InterConnectionRequestImpl that = (InterConnectionRequestImpl) o;
        return Objects.equals(route, that.route) &&
                Objects.equals(timeTableRequest, that.timeTableRequest);
    }

    @Override
    public int hashCode() {

        return Objects.hash(route, timeTableRequest);
    }
}