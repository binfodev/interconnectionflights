package com.ryanair.api.connector.clients.requests;

public interface InterConnectionRequest extends RequestTimeTable, RequestRoute {
}
