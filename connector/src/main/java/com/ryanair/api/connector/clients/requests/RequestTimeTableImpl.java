package com.ryanair.api.connector.clients.requests;

import java.util.Date;
import java.util.Objects;

public class RequestTimeTableImpl implements RequestTimeTable {

    private Date departureDate;
    private Date arrivalDate;

    RequestTimeTableImpl(Date departureDate, Date arrivalDate) {
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    @Override
    public Date getDepartureDate() {
        return new Date(departureDate.getTime());
    }

    @Override
    public Date getArrivalDate() {
        return new Date(arrivalDate.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestTimeTableImpl that = (RequestTimeTableImpl) o;
        return Objects.equals(departureDate, that.departureDate) &&
                Objects.equals(arrivalDate, that.arrivalDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(departureDate, arrivalDate);
    }
}