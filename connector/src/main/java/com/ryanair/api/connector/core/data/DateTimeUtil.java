package com.ryanair.api.connector.core.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtil {

    private static final String DATE_TIME_ISO_FOTMAT = "yyyy-MM-dd'T'HH:mm";
    private static final String DATE_FOTMAT = "yyyy-MM-dd";

    public static Integer getYear(Date date) {
        return getCalendar(date).get(Calendar.YEAR);
    }

    public static Integer getMonth(Date date) {
        return getCalendar(date).get(Calendar.MONTH);
    }

    private static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    static boolean before(Date arrival, Date departure, int hours) {
        if (arrival != null && departure != null) {
            Calendar calendar = getCalendar(arrival);
            calendar.add(Calendar.HOUR, hours);
            return calendar.getTime().before(departure);
        } else {
            return false;
        }
    }

    public static Date parseDateTime(String dateString) {
        DateFormat dateTimeFormat = new SimpleDateFormat(DATE_TIME_ISO_FOTMAT);
        try {
            return dateTimeFormat.parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Date parseDateTime(Date date, Integer month, Integer day, String time) {
        Calendar calendar = getCalendar(date);
        return parseDateTime(calendar.get(Calendar.YEAR), month, day, time);
    }

    public static Date parseDateTime(Integer year, Integer month, Integer day, String time) {
        DateFormat dateTimeFormat = new SimpleDateFormat(DATE_TIME_ISO_FOTMAT);
        DateFormat dateFformat = new SimpleDateFormat(DATE_FOTMAT);

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);

        String str = dateFformat.format(calendar.getTime());

        try {
            return dateTimeFormat.parse(str + "T" + time);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}