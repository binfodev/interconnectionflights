package com.ryanair.api.connector.core.data;

import java.util.Date;
import java.util.Objects;

public class Leg {

    private String departureAirport;
    private String arrivalAirport;
    private Date departureDateTime;
    private Date arrivalDateTime;

    public Leg(String departureAirport, Date departureDateTime, String arrivalAirport, Date arrivalDateTime) {
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureDateTime = departureDateTime;
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public Date getDepartureDateTime() {
        return new Date(departureDateTime.getTime());
    }

    public Date getArrivalDateTime() {
        return new Date(arrivalDateTime.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Leg leg = (Leg) o;
        return Objects.equals(departureAirport, leg.departureAirport) &&
                Objects.equals(arrivalAirport, leg.arrivalAirport) &&
                Objects.equals(departureDateTime, leg.departureDateTime) &&
                Objects.equals(arrivalDateTime, leg.arrivalDateTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(departureAirport, arrivalAirport, departureDateTime, arrivalDateTime);
    }

    @Override
    public String toString() {
        return "Leg{" +
                "departureAirport='" + departureAirport + '\'' +
                ", arrivalAirport='" + arrivalAirport + '\'' +
                ", departureDateTime='" + departureDateTime + '\'' +
                ", arrivalDateTime='" + arrivalDateTime + '\'' +
                '}';
    }
}