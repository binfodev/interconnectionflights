package com.ryanair.api.connector.clients;

import com.ryanair.api.connector.clients.requests.Request;

public interface Client<R extends Request,T> {

    T get(R request);
}