package com.ryanair.api.connector.core;

import com.ryanair.api.connector.clients.*;
import com.ryanair.api.connector.clients.parser.Parser;
import com.ryanair.api.connector.clients.provider.ClientProvider;
import com.ryanair.api.connector.clients.provider.ClientProviderImpl;
import com.ryanair.api.connector.clients.requests.InterConnectionRequest;
import com.ryanair.api.connector.clients.requests.RequestRoute;
import com.ryanair.api.connector.core.data.Chain;
import com.ryanair.api.connector.core.data.DataTimeTable;
import com.ryanair.api.connector.core.data.clients.Route;
import com.ryanair.api.connector.core.data.clients.TimeTable;
import com.ryanair.api.connector.core.factory.ParserFactory;
import com.ryanair.api.connector.service.InterConnectorService;
import com.ryanair.api.connector.service.InterConnectorServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.List;

@Profile("application")
@Configuration
public class AppConfiguration {

    @Bean
    public ClientProvider getRouteProvider() {
        return new ClientProviderImpl();
    }

    @Bean
    public Parser<List<Route>> getRouteParser() {
        return ParserFactory.getRoute();
    }

    @Bean
    public ClientProvider getTimeTableProvider() {
        return new ClientProviderImpl();
    }

    @Bean
    public Parser<TimeTable> getTimeTableParser() {
        return ParserFactory.getTimeTable();
    }

    @Value("${routesUrl}")
    private String routesUrl;

    @Bean("routeClient")
    public Client<RequestRoute, List<Chain>> getRouteClient() {
        return new RouteClient(routesUrl, getRouteProvider(), getRouteParser());
    }

    @Value("${schedulesUrl}")
    private String schedulesUrl;

    @Bean
    public UrlResolver getUrlResolver() {
        return new UrlResolverImpl(schedulesUrl);
    }

    @Bean("timeTableClient")
    public Client<InterConnectionRequest, DataTimeTable> getTimeTableClient() {
        return new TimeTableClient(getUrlResolver(), getTimeTableProvider(), getTimeTableParser());
    }

    @Bean("interConnectorService")
    public InterConnectorService getInterConnectorService() {
        return new InterConnectorServiceImpl(getRouteClient(), getTimeTableClient());
    }
}