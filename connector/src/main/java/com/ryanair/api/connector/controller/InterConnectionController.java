package com.ryanair.api.connector.controller;

import com.ryanair.api.connector.clients.requests.InterConnectionRequestImpl;
import com.ryanair.api.connector.core.data.InterConnection;
import com.ryanair.api.connector.service.InterConnectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.ryanair.api.connector.core.data.DateTimeUtil.parseDateTime;

@RestController
public class InterConnectionController {
    private static final Logger LOG = LoggerFactory.getLogger(InterConnectionController.class);

    private InterConnectorService service;

    public InterConnectionController(InterConnectorService service) {
        this.service = service;
    }

    @RequestMapping(value = "/interconnections", method = RequestMethod.GET)
    @ResponseBody
    public List<InterConnection> getInterConnection(@RequestParam("departure") String from, @RequestParam("arrival") String to, @RequestParam("departureDateTime") String fromDate, @RequestParam("arrivalDateTime") String toDate) {
        return service.getInterConnections(new InterConnectionRequestImpl(from, parseDateTime(fromDate), to, parseDateTime(toDate)));
    }

    @ExceptionHandler(Exception.class)
    public String handleError(HttpServletRequest req, Exception ex) {
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        return String.format("error on [%s] message: %s ", req.getRequestURL(), ex);
    }
}