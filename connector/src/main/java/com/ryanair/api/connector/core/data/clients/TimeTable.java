package com.ryanair.api.connector.core.data.clients;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeTable {

    private Integer month;
    private List<Day> days = newArrayList();

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "DataTimeTable{" +
                "month=" + month +
                ", days=" + days +
                '}';
    }
}